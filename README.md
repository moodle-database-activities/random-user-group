# Random User-Group

Random User-Group is a preset for the Moodle activity database.

## Demo


## Getting started

Download the source code and zip the files WITHOUT parent folder. Create a "Database" activity in Moodle and then upload the ZIP file in the "Presets" tab under "Import".

Import the student names and classes as a CSV file.

## Language Support

The preset is available in German. 

## Description

You can filter by class and select random students or create random groups.

## License


## Screenshots

<img width="400" alt="single view" src="/screenshots/listenansicht.png">
