// The MIT License (MIT)
// Copyright (c) 2022 by Nathan (https://codepen.io/nathanm123/pen/yLbzarr)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
// EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO 
// EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES 
// OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
// ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
// DEALINGS IN THE
// SOFTWARE.

function generateTeams () {
  document.getElementById("teams").innerHTML = '<div type="button" onclick="window.print()" style="width:100%"><i class="icon fa fa-print fa-fw"></i></div>';
  let players = document.getElementById("players").value
  players = players.split(/,/)
  players = players.filter(player => /[a-z]/i.test(player))
  const numTeams = players.length/Number(document.getElementById("num-teams").value);
  for (i = 0; i < numTeams; i++) {
    const team = document.createElement("p");
team.classList.add("ppp");
 team.contentEditable = "true";
 team.style.backgroundColor = '#'+ Math.floor(Math.random()*16777215).toString(16) + 40;
    team.innerHTML = `<span class="headteam"><b>Team ${i +1}</b></span><br>`
    teams.appendChild(team)
  }
  let curTeam = 0;
  while (players.length>0) {
    rndIndex = Math.floor(Math.random()*players.length)
    const player = players.splice(rndIndex, 1);
    const newTeam = document.getElementById("teams").getElementsByTagName("p")[curTeam];
    let playerText = document.createTextNode(`${player}\u000a`); 
    if (players.length < numTeams) {
      playerText = document.createTextNode(`${player}`)
    }
    newTeam.appendChild(playerText);
    if (curTeam < numTeams-1) {
      curTeam ++
    } else {
      curTeam = 0;
    }
  }
}

